# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)

Gem::Specification.new do |spec|
  spec.name          = "slack-purge"
  spec.version       = '1.0.0'
  spec.authors       = ["Patrick Reagan"]
  spec.email         = ["patrick.reagan@viget.com"]

  spec.summary       = "Quickly delete your Slack message history"
  spec.description   = spec.summary
  spec.homepage      = "http://www.viget.com/extend"
  spec.license       = "MIT"

  # Prevent pushing this gem to RubyGems.org by setting 'allowed_push_host', or
  # delete this section to allow pushing this gem to any host.
  if spec.respond_to?(:metadata)
    spec.metadata['allowed_push_host'] = "TODO: Set to 'http://mygemserver.com'"
  else
    raise "RubyGems 2.0 or newer is required to protect against public gem pushes."
  end

  spec.files         = ['bin/slack-purge', 'LICENSE.txt', 'README.md'] + Dir['lib/**/*.rb']
  spec.bindir        = "bin"
  spec.executables   = ['slack-purge']
  spec.require_paths = ['lib']

  spec.add_dependency 'thor',          '~> 0.19.0'
  spec.add_dependency 'activesupport', '~> 4.2'
  spec.add_dependency 'httparty',      '~> 0.16.0'

  spec.add_development_dependency "bundler", "~> 1.10"
end
