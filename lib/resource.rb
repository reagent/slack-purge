module Resource
  def initialize(attributes = {})
    attributes.each do |attribute_name, value|
      setter_method_name = "#{attribute_name}="
      send(setter_method_name, value) if respond_to?(setter_method_name)
    end
  end
end
