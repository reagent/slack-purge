class Response
  def self.for(method_name, options = {})
    response = HTTParty.get("https://slack.com/api/#{method_name}", {
      query: options.merge(token: Configuration.token)
    })

    response.parsed_response
  end
end
