class Messages
  include Enumerable
  extend Forwardable

  def_delegators :asc, :each

  alias_method :desc, :to_a

  def initialize(target, options = {})
    @target  = target # can be a channel or a DM conversation
    @options = options
    @to      = nil
  end

  def for(user)
    for_user_id(user.id)
  end

  def for_user_id(user_id)
    @user_id = user_id
    self
  end

  def from(time)
    @options.merge!(oldest: time.to_f)
    self
  end

  def to(time)
    @to = time.to_f
    self
  end

  def in_batches_of(count)
    @options.merge!(count: count)

    if block_given?
      scope         = self
      current_count = 1

      loop do
        scope.each do |message|
          return self if @to.present? && message.newer_than?(@to)

          yield(message, current_count)
          current_count += 1
        end

        return self unless scope.more?

        scope = scope.next_batch(scope.latest_timestamp)
      end
    else
      self
    end
  end

  def asc
    to_a.sort
  end

  def to_a
    @to_a ||= @user_id ? messages.select {|m| m.user_id == @user_id } : messages
  end

  def count
    to_a.length
  end

  def latest_timestamp
    response['latest'] || messages.first.timestamp
  end

  def oldest_timestamp
    response['oldest'] || messages.last.timestamp
  end

  def next_batch(since)
    messages = self.class.new(@target, options)
    messages = messages.from(since)           if since.present?
    messages = messages.for_user_id(@user_id) if @user_id.present?
    messages
  end

  def more?
    response['has_more']
  end

  private

  def messages
    @messages ||= response['messages'].map {|r| Message.new(@target, r) }
  end

  def options
    default_options.merge(@options)
  end

  def default_options
    {
      count:     1000,
      inclusive: 0,
      channel:   @target.id
    }
  end

  def response
    @response ||= Response.for(api_method_name, options)
  end

  def api_method_name
    case @target
    when Channel
      'conversations.history'
    when Conversation
      'im.history'
    else
      raise ArgumentError.new('Unknown target type')
    end
  end
end
