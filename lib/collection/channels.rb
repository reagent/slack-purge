class Channels
  def self.all
    @all ||= begin
      response = Response.for('conversations.list', {
        types: "public_channel,private_channel",
        limit: 1000
      })

      response['channels'].map {|r| Channel.new(r) }
    end
  end
end
