class Users
  def self.all
    @all ||= begin
      response = Response.for('users.list')
      response['members'].map {|r| User.new(r) }
    end
  end
end
