class Conversations

  def self.all
    @all ||= begin
      response = Response.for('im.list')
      response['ims'].map {|r| Conversation.new(r) }
    end
  end

end