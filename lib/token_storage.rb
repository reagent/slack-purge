class TokenStorage
  attr_reader :error

  def self.storage_path
    Pathname.new(ENV['HOME']).join('.slack-purge')
  end

  def self.find(label)
    YAML.load_file(storage_path)[label.to_s]
  end

  def initialize(label, token)
    @label = label
    @token = token
    @error = nil
  end

  def save
    valid? && create_storage_file && add_token
  end

  private

  def to_hash
    YAML.load_file(storage_path)
  end

  def storage_path
    self.class.storage_path
  end

  def valid?
    @error = 'label missing' if @label.blank?
    @error = 'token missing' if @token.blank?

    @error.nil?
  end

  def create_storage_file
    storage_path.open('w') {|f| f << YAML.dump({}) } unless storage_path.exist?
    true
  end

  def add_token
    data = to_hash.merge(@label.to_s => @token)
    storage_path.open('w') {|f| f << YAML.dump(data) }
    true
  end

end