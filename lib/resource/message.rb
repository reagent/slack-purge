class Message
  include Resource

  attr_accessor :type, :text
  attr_writer   :user, :ts

  def initialize(channel, attributes = {})
    @channel = channel
    super(attributes)
  end

  def user_id
    @user
  end

  def timestamp
    @ts.to_f
  end

  def user
    User.find(user_id)
  end

  def delete
    response = Response.for('chat.delete', ts: @ts, channel: @channel.id)
    response['ok']
  end

  def newer_than?(timestamp)
    self.timestamp > timestamp
  end

  def posted_at
    Time.at(timestamp.to_f)
  end

  def <=>(other)
    timestamp <=> other.timestamp
  end
end
