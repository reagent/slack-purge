class User
  include Resource

  attr_writer :id, :user_id, :user, :name, :real_name

  def self.current
    @current ||= new(Response.for('auth.test'))
  end

  def self.find_by_name(name)
    Users.all.detect {|u| u.has_handle?(name) }
  end

  def self.find(id)
    Users.all.detect {|u| u.id == id }
  end

  def id
    @id || @user_id
  end

  def name
    @name || @user
  end

  def full_name
    @real_name
  end

  def has_handle?(handle)
    [name, full_name].include?(handle)
  end
end
