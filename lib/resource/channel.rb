class Channel
  include Resource

  attr_accessor :id, :name
  attr_writer   :created

  def self.find_by_name(name)
    Channels.all.detect {|c| c.name == name }
  end

  def created_ts
    @created
  end

  def created_at
    Time.at(created_ts.to_f)
  end

  def messages
    Messages.new(self).from(created_ts)
  end
end
