class Conversation
  include Resource

  attr_writer   :user, :channel, :created
  attr_accessor :id

  def self.find_by_recipient(name)
    Conversations.all.detect {|c| c.user.has_handle?(name) }
  end

  def channel_id
    @channel
  end

  def user
    User.find(@user)
  end

  def created_ts
    @created
  end

  def created_at
    Time.at(created_ts.to_f)
  end

  def messages
    Messages.new(self).from(created_ts)
  end

end