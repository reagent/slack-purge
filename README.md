# Slack Purge

Delete your message history from slack channels or conversations

## Local Installation

    $ git clone https://gitlab.com/reagent/slack-purge.git
    $ cd slack-purge && gem build slack-purge.gemspec && gem install slack-purge*.gem

## Usage

Before you can use the API, you'll need to sign into your Slack workspace and then generate a [Legacy API Token](https://api.slack.com/custom-integrations/legacy-tokens).  Once that's complete, you can initialize your API token with an alias and value:

    $ slack-purge auth mycompany d3adb33f

You can then clear your history for a selected channel and be prompted to remove each entry:

    $ slack-purge delete mycompany general --until=2015-01-01
    $ slack-purge delete mycompany general --count=100

Or you can skip prompts for each message with the `--force` flag:

    $ slack-purge delete mycompany general --count=100 --force

Similarly, you can delete the history for direct messages with another user using either their handle or full name:

    $ slack-purge delete mycompany jdoe --count=100
    $ slack-purge delete mycompany 'John Doe' --count=100

Keep in mind that these commands will only delete the history for the user associated with the token, it will not be
able to delete others' messages.
